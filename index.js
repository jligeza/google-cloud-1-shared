exports.getStorageUserImageName = (email, timestamp, fileName) => {
  return `${email}_${timestamp}_${fileName}`
}
